//
//  MovieCatalogTableViewDelegate.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/16/1399 AP.
//

import UIKit

extension MovieCatalogViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.startIndicator()
        let movie = filteredMovies[indexPath.row]
        self.apiService.fetchMovieDetail(ForMovieID: movie.id) { movieDetail, errorString  in
            self.stopIndicator()
            guard errorString == nil else {
                self.movieTableView.selectRow(at: nil, animated: true, scrollPosition: .none) // To remove selection from selected cell
                self.showAlert(alertText: "Warning", alertMessage: errorString!)
                return
            }
            
            guard let movieDetail = movieDetail else { return }
            let movieDetailVC   = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "movieDetailViewController") as! MovieDetailViewController
            
            movieDetailVC.movieDetail = movieDetail
            
            self.navigationController?.pushViewController(movieDetailVC, animated: true)
        }
    }
}
