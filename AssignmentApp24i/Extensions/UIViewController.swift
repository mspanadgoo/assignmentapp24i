//
//  UIViewController.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/16/1399 AP.
//

import UIKit

extension UIViewController {
    func showAlert(alertText : String, alertMessage : String) {
        let alert = UIAlertController(title: alertText, message: alertMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func startIndicator() {
        let indicator       = UIActivityIndicatorView(frame: .init(x: 0, y: 0, width: 50, height: 50))
        let indicatorView   = UIView(frame: CGRect(x: 0, y: 0, width: 150, height: 100))
        
        indicatorView.tag                   = -334455
        indicatorView.backgroundColor       = .lightGray
        indicatorView.alpha                 = 0.6
        indicatorView.center                = self.view.center
        indicatorView.layer.cornerRadius    = 10
        
        indicator.tag                       = -334456
        indicator.color                     = .white
        indicator.style                     = .whiteLarge
        indicator.center                    = self.view.center
        indicator.startAnimating()

        self.view.addSubview(indicatorView)
        self.view.addSubview(indicator)
    }
    
    func stopIndicator() {
        guard let indicatorView = self.view.viewWithTag(-334455) else { return }
        guard let indicator     = self.view.viewWithTag(-334456) else { return }
        
        indicatorView.removeFromSuperview()
        indicator.removeFromSuperview()
    }
}
