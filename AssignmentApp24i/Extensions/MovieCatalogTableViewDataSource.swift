//
//  MovieCatalogTableViewDataSource.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/16/1399 AP.
//

import UIKit

extension MovieCatalogViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell    = tableView.dequeueReusableCell(withIdentifier: "movieTableViewCell", for: indexPath) as! MovieTableViewCell
        let movie   = filteredMovies[indexPath.row]
        
        cell.movieTitleLabel.text = movie.title
        
        self.apiService.fetchPoster(imageName: movie.posterPath) { (data) in
            guard let data = data else { return }
            cell.moviePosterImageView.image = UIImage(data: data, scale: 1)
        }
        return cell
    }
}
