//
//  MovieCatalogSearchBarDelegate.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/16/1399 AP.
//

import UIKit

extension MovieCatalogViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filteredMovies = searchText.isEmpty ? self.movies : self.movies.filter { $0.title.contains(searchText) }
        self.reloadMovieTableView()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}
