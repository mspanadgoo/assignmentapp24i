//
//  APIService.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/16/1399 AP.
//
import SystemConfiguration
import Foundation
import Alamofire

class APIService: NSObject {
    
    private let apiKey                  = "1153b711755a32ecf2bba2be3d883065"
    private let sourceURLString         = "https://api.themoviedb.org/3/movie/"
    private let postersBaseURLString    = "http://image.tmdb.org/t/p/w342"
    
    func fetchMovies(completion: @escaping (Movies?, String?) -> Void) {
        guard self.isConnectedToNetwork() else {
            completion(nil,"No network connection")
            return
        }
        
        AF.request(self.sourceURLString + "popular?api_key=" + self.apiKey)
            .responseDecodable(of: Movies.self) { response in
                guard let movies = response.value else {
                    completion(nil, "Failed to load")
                    return
                }
                if movies.results.count > 0 {
                    completion(movies, nil)
                } else {
                    completion(movies, "Failed to load")
                }
            }
    }
    
    func fetchMovieDetail(ForMovieID id: Int, completion: @escaping (MovieDetail?, String?) -> Void) {
        guard self.isConnectedToNetwork() else {
            completion(nil,"No network connection")
            return
        }
        
        AF.request("\(self.sourceURLString)\(id)?api_key=\(self.apiKey)")
            .responseDecodable(of: MovieDetail.self) { response in
                guard let movieDetail = response.value else {
                    completion(nil, "Failed to load")
                    return
                }
                completion(movieDetail, nil)
            }
    }
    
    func fetchPoster(imageName: String?, completion: @escaping (Data?) -> Void ) {
        guard let imageName = imageName else { return }
        AF.request(self.postersBaseURLString + imageName)
            .response { (response) in
                switch response.result {
                case .success(let responseData):
                    completion(responseData)
                    
                case .failure(let error):
                    completion(nil)
                    print("error--->",error)
                }
            }
    }
    
    func fetchYouTubeID(ForMovieID id: Int, completion: @escaping (String?, String?) -> Void ) {
        guard self.isConnectedToNetwork() else {
            completion(nil,"No network connection")
            return
        }
        
        AF.request("\(self.sourceURLString)\(id)/videos?api_key=\(self.apiKey)")
            .responseDecodable(of: Videos.self) { response in
                guard let video = response.value else {
                    completion(nil, "Failed to load")
                    return
                }
                guard video.results.count > 0 else {
                    completion(nil, "Failed to load")
                    return
                }
                completion(video.results.first!.key, nil)
            }
    }
    
    func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
}
