//
//  MovieDetail.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct MovieDetail: Decodable {
    let posterPath          : String?
    let overview            : String?
    let releaseDate         : String
    let title               : String
    let id                  : Int
    let genres              : [Genres]
    
    // Not needed
//    let belongsToCollection : String?
//    let backdropPath        : String?
//    let tagline             : String?
//    let status              : String?
//    let imdbID              : String?
//    let originalLanguage    : String
//    let originalTitle       : String
//    let homepage            : String
//    let voteAverage         : Float
//    let budget              : Int
//    let revenue             : Int
//    let runtime             : Int
//    let voteCount           : Int
//    let spokenLanguages     : [SpokenLanguage]
//    let productionCompanies : [ProductionCompany]
//    let video               : Bool
//    let adult               : Bool
    
    enum CodingKeys: String, CodingKey {
        case posterPath             = "poster_path"
        case overview
        case releaseDate            = "release_date"
        case title
        case id
        case genres
        
        // Not needed
//        case belongsToCollection    = "belongs_to_collection"
//        case backdropPath           = "backdrop_path"
//        case tagline
//        case status
//        case imdbID                 = "imdb_id"
//        case originalLanguage       = "original_language"
//        case originalTitle          = "original_title"
//        case homepage
//        case voteAverage            = "vote_average"
//        case budget
//        case revenue
//        case runtime
//        case voteCount              = "vote_count"
//        case spokenLanguages        = "spoken_languages"
//        case productionCompanies    = "production_companies"
//        case video
//        case adult
    }
}
