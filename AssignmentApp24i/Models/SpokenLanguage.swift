//
//  SpokenLanguage.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct SpokenLanguage: Decodable {
    let iso     : String
    let name    : String
    
    enum CodingKeys: String, CodingKey {
        case iso = "iso_639_1"
        case name
    }
}
