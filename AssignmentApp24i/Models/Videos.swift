//
//  Videos.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct Videos: Decodable {
    let id      : Int
    let results : [VideoResult]
    
    enum CodingKeys: String, CodingKey {
        case id
        case results
    }
}
