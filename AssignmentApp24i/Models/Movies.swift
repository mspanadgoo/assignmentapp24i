//
//  Movies.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct Movies: Decodable {
    let page            : Int
    let results         : [Movie]
    let totalResults    : Int
    let totalPages      : Int
    
    enum CodingKeys: String, CodingKey {
        case page
        case results
        case totalResults   = "total_results"
        case totalPages     = "total_pages"
    }
}
