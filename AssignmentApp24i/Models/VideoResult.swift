//
//  VideoResult.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct VideoResult: Decodable {
    let id          : String
    let iso639      : String
    let iso3166     : String
    let key         : String
    let name        : String
    let site        : String
    let type        : String
    let size        : Int
    
    enum CodingKeys: String, CodingKey {
        case id
        case iso639     = "iso_639_1"
        case iso3166    = "iso_3166_1"
        case key
        case name
        case site
        case type
        case size
    }
}
