//
//  Movie.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct Movie: Decodable {
    let backdropPath        : String?
    let posterPath          : String?
    let overview            : String
    let releaseDate         : String
    let originalTitle       : String
    let originalLanguage    : String
    let title               : String
    let voteAverage         : Float
    let popularity          : Float
    let id                  : Int
    let voteCount           : Int
    let genreIDs            : [Int]
    let video               : Bool
    let adult               : Bool
    
    enum CodingKeys: String, CodingKey {
        case backdropPath       = "backdrop_path"
        case posterPath         = "poster_path"
        case overview
        case releaseDate        = "release_date"
        case originalTitle      = "original_title"
        case originalLanguage   = "original_language"
        case title
        case voteAverage        = "vote_average"
        case popularity
        case id
        case voteCount          = "vote_count"
        case genreIDs           = "genre_ids"
        case video
        case adult
    }
}
