//
//  ProductionCompany.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import Foundation

struct ProductionCompany: Decodable {
    let name            : String
    let id              : Int
    let logoPath        : String?
    let originCountry   : String
    
    enum CodingKeys: String, CodingKey {
        case name
        case id
        case logoPath       = "logo_path"
        case originCountry  = "string"
    }

}
