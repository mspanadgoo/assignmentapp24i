//
//  MovieTableViewCell.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/14/1399 AP.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var moviePosterImageView : UIImageView!
    @IBOutlet weak var movieTitleLabel      : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
