//
//  MovieCatalogViewController.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/14/1399 AP.
//

import UIKit

class MovieCatalogViewController: UIViewController {
    
    @IBOutlet weak var movieTableView   : UITableView!
    @IBOutlet weak var searchBar        : UISearchBar!
    @IBOutlet weak var noDataLabel      : UILabel!
    
    var apiService                      : APIService!
    var movies                          : [Movie]!
    var filteredMovies                  : [Movie]!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initial()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.movieTableView.selectRow(at: nil, animated: true, scrollPosition: .none) // To remove selection from selected cell
    }
    
    //MARK: Custom Methods
    func initial() {
        self.apiService     = APIService()
        self.movies         = [Movie]()
        self.filteredMovies = [Movie]()
        
        self.fetchMovies()
    }
    
    func reloadMovieTableView() {
        self.noDataLabel.isHidden = self.filteredMovies.count > 0
        self.movieTableView.reloadData()
    }
    
    func fetchMovies() {
        self.startIndicator()
        self.movies.removeAll()
        self.filteredMovies.removeAll()
        self.reloadMovieTableView()
        
        self.apiService.fetchMovies { movies, errorString in
            self.stopIndicator()
            guard errorString == nil else {
                self.showAlert(alertText: "Warning", alertMessage: errorString!)
                return
            }
            
            guard let movies = movies else { return }
            self.movies         = movies.results
            self.filteredMovies = movies.results // FilterdMovies and Movies both contain all movies
            self.reloadMovieTableView()
        }
    }
    
    //MARK: Actions
    @IBAction func refreshButtonTapped(sender: Any) {
        self.fetchMovies()
    }
}
