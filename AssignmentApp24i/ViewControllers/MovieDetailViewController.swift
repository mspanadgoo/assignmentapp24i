//
//  MovieDetailViewController.swift
//  AssignmentApp24i
//
//  Created by Mohammad Sadegh Panadgoo on 7/15/1399 AP.
//

import UIKit
import AVKit
import XCDYouTubeKit

class MovieDetailViewController: UIViewController {
    
    @IBOutlet weak var moviePosterImageView : UIImageView!
    @IBOutlet weak var movieTitleLabel      : UILabel!
    @IBOutlet weak var watchTrailerButton   : UIButton!
    @IBOutlet weak var genresDetailLabel    : UILabel!
    @IBOutlet weak var dateDetailLabel      : UILabel!
    @IBOutlet weak var overviewDetailLabel  : UILabel!
    
    var apiService                          : APIService!
    var movieDetail                         : MovieDetail!
    var playerVC                            : AVPlayerViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initial()
    }
    
    //MARK: Custom Methods
    func initial() {
        self.apiService = APIService()
        self.playerVC   = AVPlayerViewController()
        
        self.apiService.fetchPoster(imageName: self.movieDetail.posterPath) { data in
            guard let data = data else { return }
            self.moviePosterImageView.image = UIImage(data: data, scale: 1)
        }
        
        self.movieTitleLabel.text       = self.movieDetail.title
        self.genresDetailLabel.text     = self.getGenresString(forGenres: self.movieDetail.genres)
        self.dateDetailLabel.text       = self.movieDetail.releaseDate
        self.overviewDetailLabel.text   = self.movieDetail.overview
    }
    
    func getGenresString(forGenres genres: [Genres]) -> String {
        guard genres.count > 0 else { return "" } 
        var genresString = ""
        for genre in genres {
            genresString += genre.name
            genresString += genres.last!.id != genre.id ? ", " : "" //last one doesn't need ','
        }
        return genresString
    }

    //MARK: Actions
    @IBAction func watchTrailerButtonTapped(sender: Any) {
        self.startIndicator()
        self.apiService.fetchYouTubeID(ForMovieID: self.movieDetail.id) { youTubeID, errorString in
            self.stopIndicator()
            guard errorString == nil else {
                self.showAlert(alertText: "Warning", alertMessage: errorString!)
                return
            }
            
            self.present(self.playerVC, animated: true, completion: nil)
            
            XCDYouTubeClient.default().getVideoWithIdentifier(youTubeID) { (video, error) in
                
                guard let video: XCDYouTubeVideo = video else {
                    self.playerVC.dismiss(animated: true, completion: nil)
                    return
                }
                
                self.playerVC.player = AVPlayer(url: video.streamURL!)
                self.playerVC.player!.play() // To automatically start playback
                
                NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.playerVC.player!.currentItem) // To find out when trailer is finished
            }
        }
    }
    
    @objc func playerDidFinishPlaying() {
        self.playerVC.dismiss(animated: true, completion: nil)
    }
}
